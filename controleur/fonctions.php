<?php include('../model/connexionDB.php'); ?>
<?php 

function verifAdmin($nom)
{

    global $pdo;
    $req = $pdo->prepare("SELECT * FROM Administrateur where nom=?;");
    $req->execute([$nom]);
    return $req->fetch();
}



 /*  <!-- créer une fonction qui ajoute une photo --> */

 


 function addImage($nom,$chemin)
{
    global $pdo;
    $req = $pdo->prepare("insert into images (nom,chemin,visibilite) values (?, ?, 1);");
    $req->execute([$nom, $chemin]);
}

  /* <!-- créer une fonction qui rend une photo invisible --> */
 
function ImageInvisible($nom)
{
    global $pdo;
    $req = $pdo->prepare("Update images SET visibilite = 0 where nom = ?;");
    $req->execute([$nom]);
}

/* <!-- créer une fonction qui rend une photo visible --> */

function ImageVisible($nom)
{
    global $pdo;
    $req = $pdo->prepare("Update images SET visibilite = 1 where nom = ?;");
    $req->execute([$nom]);
}


  /* créer une fonction qui recupere toutes les photos visibles--> */
  function PrendreImagesVisibles()
  {
      global $pdo;
      $req = $pdo->query('select * from images where visibilite=1  AND nom !="Prix";');
      return $req->fetchAll();
  }

  function PrendreVideosVisibles()
  {
      global $pdo;
      $req = $pdo->query('select * from videos where visibilite=1 ;');
      return $req->fetchAll();
  }
  function afficherImgIndex()
  {
      global $pdo;
      $req = $pdo->query('select * from images where visibilite=1  ORDER by id DESC limit 3;');
      return $req->fetchAll();
  }

  function rubriquecouteaux()
  {
      global $pdo;
      $req = $pdo->query('select * from Prix where rubrique="couteaux" and visibilite=1 ');
      return $req->fetchAll();
  }



  function rubriqueciseaux()
  {
      global $pdo;
      $req = $pdo->query('select * from Prix where rubrique="ciseaux"and visibilite=1 ');
      return $req->fetchAll();
  }

  function rubriquetravaildubois()
  {
      global $pdo;
      $req = $pdo->query('select * from Prix where rubrique="travaildubois" and visibilite=1');
      return $req->fetchAll();
  }
 function rubriquejardin()
  {
      global $pdo;
      $req = $pdo->query('select * from Prix where rubrique="jardin" and visibilite=1 ');
      return $req->fetchAll();
  }

  /* <!-- créer une fonction qui ajoute une video--> */

  function addVideo($nom,$lien )
  {
      global $pdo;
      $req = $pdo->prepare("insert into videos (nom, lien, visibilite) values (?, ?, 1);");
      $req->execute([$nom,$lien]);
  }


   /* <!-- créer une fonction qui rend la video invisible--> */

   function VideoInvisible($nom)
   {
       global $pdo;
       $req = $pdo->prepare("Update  SET visibilite = 0 where nom = ?;");
       $req->execute([$nom]);
   }
   

  
  
 
   

/*     <!-- créer une fonction qui ajoute une actualité--> */
function addActu($texte,$images )
{
    global $pdo;
    $req = $pdo->prepare("insert into actualite (texte, images) values (?, ?, 1);");
    $req->execute([$texte, $images, ]);
}

     /* <!-- créer une fonction qui rend actu invisible --> */

     function actuInvisible($texte)
     {
         global $pdo;
         $req = $pdo->prepare("UPDATE  SET visibilite = 0 where nom = ?;");
         $req->execute([$texte]);
     }
     /*  <!-- créer une fonction qui récupère toutes les actu visibles--> */
     function PrendreActuVisible()
   {
       global $pdo;
       $req = $pdo->query('select * from actualite where visibilite=1;');
       return $req->fetchAll();
   }

     /*  <!-- créer une fonction qui modifie les contacts--> */
     function modifInformations($adresse, $telephone,$mail, $horaires)
     {
        global $pdo;
         $req = $pdo->prepare("UPDATE coordonnees SET adresse = ?, telephone = ?,mail = ?; horaires = ?;");
         $req->execute([$adresse, $telephone,$mail, $horaires]);
     }
     
     

       /*  <!-- créer une fonction qui -->*/
       function getInfos()
       {
           global $pdo;
           $req = $pdo->prepare('Select * from coordonnees;');
           $req->execute();
           return $req->fetch();
       }
       function getPrix(){
        global $pdo;
        $req = $pdo->prepare('Select * from Prix;');
        $req->execute();
        return $req->fetch();
       }
       function addPrix($rubrique,$prestations,$prix){
        global $pdo;
        $req = $pdo->prepare('INSERT INTO  Prix(rubrique,prestations,prix,visibilite) values (?,?, ?, 1) ;');
        $req->execute([$rubrique,$prestations,$prix]);

       }
    function serviceInvisible($prestations)
{
    global $pdo;
    $req = $pdo->prepare("Update Prix SET visibilite = 0 where prestations = ?;");
    $req->execute([$prestations]);
}

       

        /*  <!-- créer une fonction qui --> */
         
function modifimagedesprix($chemin)
{

    global $pdo;
    $req = $pdo->prepare( "UPDATE images SET chemin='?' where nom='Prix' ;");
    $req->execute([$chemin]);
}

/**** [ Fonction pour admin.php page apropos ] ****/ 

function changeh1Propos()
       {
           global $pdo;
           $req = $pdo->prepare('SELECT h1 FROM apropos ORDER BY id DESC LIMIT 1;');
           $req->execute();
           return $req->fetch();
       }
function changePara1Propos()
       {
           global $pdo;
           $req = $pdo->prepare('SELECT para1 FROM apropos ORDER BY id DESC LIMIT 1;');
           $req->execute();
           return $req->fetch();
       }
       function img1Propos()
       {
           global $pdo;
           $req = $pdo->prepare('SELECT img1 FROM apropos ORDER BY id DESC LIMIT 1;');
           $req->execute();
           return $req->fetch();
        }
        function changePara2Propos()
        {
            global $pdo;
            $req = $pdo->prepare('SELECT para2 FROM apropos ORDER BY id DESC LIMIT 1;');
            $req->execute();
            return $req->fetch();
        }
        function img2Propos()
       {
           global $pdo;
           $req = $pdo->prepare('SELECT img2 FROM apropos ORDER BY id DESC LIMIT 1;');
           $req->execute();
           return $req->fetch();
        }
        function changePara3Propos()
        {
            global $pdo;
            $req = $pdo->prepare('SELECT para3 FROM apropos ORDER BY id DESC LIMIT 1;');
            $req->execute();
            return $req->fetch();
        }
            
        
/**** [ Fonction pour actualite.php page actualites ] ****/ 
function getAllIdActu()
  {
      global $pdo;
      $req = $pdo->query('SELECT * from actualite where id;');
      return $req->fetchAll();
  }
function newTexteDateActu()
  {
      global $pdo;
      $req = $pdo->query('SELECT * from actualite where texteDateActu;');
      return $req->fetchAll();
  }
function newImageActu()
  {
      global $pdo;
      $req = $pdo->query('SELECT * from actualite where imageActu;');
      return $req->fetchAll();
  }
function newDescriptionActu()
  {
      global $pdo;
      $req = $pdo->query('SELECT * from actualite where texteDescriptionActu;');
      return $req->fetchAll();
  }
function newTitreArticle()
  {
      global $pdo;
      $req = $pdo->query('SELECT * from actualite where titreArticle;');
      return $req->fetchAll();
  }
