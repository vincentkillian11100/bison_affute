<?php
    include "../model/connexionDB.php";

    if(
        isset($_POST["h1Propos"]) &&
        isset($_POST["story1"]) &&
        isset($_POST["imgfile1"]) &&
        isset($_POST["story2"]) &&
        isset($_POST["imgfile2"]) &&
        isset($_POST["story3"]) 
        ){
            $h1Propos = htmlspecialchars($_POST["h1Propos"]); 
            $story1 = htmlspecialchars($_POST["story1"]); 
            $imgfile1 = htmlspecialchars($_POST["imgfile1"]); 
            $story2 = htmlspecialchars($_POST["story2"]); 
            $imgfile2 = htmlspecialchars($_POST["imgfile2"]); 
            $story3 = htmlspecialchars($_POST["story3"]); 

            $insertion=$pdo->prepare("INSERT INTO apropos VALUES(NULL,:h1,:para1,:img1,:para2,:img2,:para3)"); 

            $insertion->bindValue(":h1",$h1Propos);
            $insertion->bindValue(":para1",$story1);
            $insertion->bindValue(":img1",$imgfile1);
            $insertion->bindValue(":para2",$story2);
            $insertion->bindValue(":img2",$imgfile2);
            $insertion->bindValue(":para3",$story3);
            
            $verification=$insertion->execute();
        
            if ($verification) {
                
                header('Location: ../vue/modifProposOk.php');
                exit;
            } elseif ($verification === FALSE) {
                echo "Échec d'insertion" . "<br>";
            } else {
                echo "Une variable n'est pas déclarée ou est null";
            }
            }

?>