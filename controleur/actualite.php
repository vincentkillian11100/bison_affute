<?php
    include "../model/connexionDB.php";

    if(
        isset($_POST["dateArticleActu"]) &&
        isset($_POST["imageArticleActu"]) &&
        isset($_POST["titreArticle"]) &&
        isset($_POST["texteDescriptionActu"])
        ){
            $visibilite = 1;
            $dateArticleActu = htmlspecialchars($_POST["dateArticleActu"]); 
            $originalDate = $dateArticleActu;
            $timestamp = strtotime($originalDate); 
            $newDate = date("m-d-Y", $timestamp );

            $imageArticleActu = htmlspecialchars($_POST["imageArticleActu"]); 
            $texteDescriptionActu = htmlspecialchars($_POST["texteDescriptionActu"]); 
            $titreArticle = htmlspecialchars($_POST["titreArticle"]); 

            $insertion=$pdo->prepare("INSERT INTO actualite VALUES(NULL,:titreArticle,:texteDateActu,:imageActu,:texteDescriptionActu,:visibilite)"); 

            $insertion->bindValue(":titreArticle",$titreArticle);
            $insertion->bindValue(":texteDateActu",$newDate);
            $insertion->bindValue(":imageActu",$imageArticleActu);
            $insertion->bindValue(":texteDescriptionActu",$texteDescriptionActu);
            $insertion->bindValue(":visibilite",$visibilite);

            
            $verification=$insertion->execute();
        
            if ($verification) {
                
                header('Location: ../vue/ajoutArticleOk.php');
                exit;
            } elseif ($verification === FALSE) {
                echo "Échec d'insertion" . "<br>";
            } else {
                echo "Une variable n'est pas déclarée ou est null";
            }
            }

?>

