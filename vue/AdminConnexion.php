<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="AdminConnexion.css">
    <link rel="apple-touch-icon" sizes="180x180" href="../dossier/favicon_io/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../dossier/favicon_io//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../dossier/favicon_io//favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <title>Connexion</title>
</head>

<body>

<div id="containerLogin">
    <form class="FormAdmin" id="centrer" action="../controleur/login.php" method="post">
        <h1 class = "connexionH1"> Connexion </h1>
        <p class = "login">Votre login :</p> <input class = "input1" type="text" name="username" required>
        <p class = "mdp">Votre mot de passe :</p> <input class = "input1" type="password" name="psw" required>
        <input class="btncommande" type="submit" value="Connexion">
    </form>
</div>

</body>

</html>