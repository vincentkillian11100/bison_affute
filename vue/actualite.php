
<title><?php echo "Actualités"; ?></title>
<?php
    include "../controleur/fonctions.php";
?>
<?php
    $getAllIdActu = getAllIdActu();
?>
<?php
    include "header.php";
?>   
<div id="bodyActuDiv">
    <div id="derniereActu">
        <div class="news">
            <p class="titreActu" >Dernières actualités</p>
        </div>
    </div>
    <main class="mainActu">
        <?php
            foreach ($getAllIdActu as $i) {
        ?>
        <div class="containerNewArticle">
            <div class="border"></div>
            <p class="titreActu2"><?php echo  $i["titreArticle"]; ?></p>
            <p class="dateActu"><?php echo  $i["texteDateActu"]; ?></p>
            <img class="imgActu" src="../images/<?php  echo $i["imageActu"]; ?>" alt="Image actualite">
            <p class="textActu"><?php echo $i["texteDescriptionActu"]; ?></p>
        </div>
        <?php } ?>
    </main>
</div>
<?php
    include "footer.php";
?>  
