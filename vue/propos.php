<title><?php echo "A propos de Bison Affûté"; ?></title>
<?php
    include "../controleur/fonctions.php";
?>
<?php 
    include 'header.php';
?>
<div id="bodyProposDiv">
<main class="mainPropos">     
    <?php
        $h1 = changeh1Propos()[0];
        $para1 = changePara1Propos()[0]; 
        $img1 = img1Propos()[0]; 
        $para2 = changePara2Propos()[0]; 
        $img2 = img2Propos()[0]; 
        $para3 = changePara3Propos()[0]; 
    ?>
<p class="h1Apropos"><?php echo $h1; ?></p>
<p class="textPropos">
<?php
    echo $para1;
?>
</p>
<img
    class="imgPropos"
    src="<?php echo "../images/" . $img1 ?>"
    alt="Premiere image"
/>
<p class="textPropos"><?php echo $para2 ?></p>
<img
    class="imgPropos"
    src="<?php echo "../images/" . $img2 ?>"
    alt="Deuxieme image"
/>
<p class="textPropos"><?php echo $para3 ?></p>
</main>
</div>
<?php
    include 'footer.php';
?>
