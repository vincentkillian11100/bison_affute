<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="ajoutArticleOk.css" />
        <script
            defer
            src="https://kit.fontawesome.com/c07901787e.js"
            crossorigin="anonymous"
        ></script>
        
        <title>Votre article a été ajouté avec succès !</title>
    </head>
    <body>
    <?php
        include 'header.php';
    ?>
        <div id="containerModif">
            <p>Votre article a été ajouté avec succès !</p>
            <i class="fas fa-check-square"></i>
            <br />
        </div>

        
        
        
    </body>
</html>
