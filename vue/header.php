<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bison Affûté">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="service.css">
    <link rel="stylesheet" href="galerie.css">
    <link rel="stylesheet" href="contact.css">
    <link rel="stylesheet" href="actualite.css">
    <link rel="stylesheet" href="propos.css">
    <link rel="stylesheet" href="index.css">
    <link rel="apple-touch-icon" sizes="180x180" href="../dossier/favicon_io/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../dossier/favicon_io//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../dossier/favicon_io//favicon-16x16.png">
    <meta name="description" 
    content="Monsieur Marc Avérous, 
    affûteur rémouleur situé dans le département de l'Aude plus précisément sur la commune de Saint-Nazaire-d'Aude.
    Propose un service de Rémoulage Affûtage pour vos vos couteaux, ciseaux, ciseaux à bois, sécateurs, cisailles haies,
    taille-haies thermiques ou électriques, les lames de coupe jambon, les chaînes de tronçonneuses,
    tous les outils de jardin.
    ">
    <title>Document</title>
</head>
<body>
<header class=''>
    <div class="header">
   <div class="container_logo_header">
   <div class="containerLeft">
       <div class="containerL_childeTop">
          <p><span class="borderYellow_header">BISON</span></p>
       </div>
       <div class="containerL_childeLow">
       <p>AFFÛTEUR</p>
       </div>
       </div>
   <div class="boxLogo">
       <img class="logo_header" src="../images/android-chrome192.png" alt="">
   </div>
   <div class="containerRight">
       <div class="containerR_childeTop">
          <p><span class="borderYellow_header">AFFÛTÉ</span></p>
       </div>
       <div class="containerR_childeLow">
     <p>RÉMOULEUR</p>
       </div>
   </div>
   </div>
   <div id="cont">
  <div class="l l1"></div>
  <div class="l l2"></div>
  <div class="l l3"></div>
   </div>
   <nav id="nav">
       <ul id="ulHeader" class='list_header ulHeader'>
           <li><a href="index.php">Accueil</a></li>
           <li><a href="services.php">Mes services</a></li>
           <li><a href="propos.php">A propos</a></li>
           <li><a href="galerie.php">Galerie</a></li>
           <li><a href="contact.php">Contact</a></li>
           <li><a href="actualite.php">Actualites</a></li>
       </ul>
   </nav>
   </div>
   <div id="CHH" class="containerHeaderHidden">
   <ul id="ulHiddenHeader" class='listHideen_header ulHeader'>
           <li class="li_header_hideen"><a href="index.php">Accueil</a></li>
           <li class="li_header_hideen"><a href="services.php">Mes services</a></li>
           <li class="li_header_hideen"><a href="propos.php">A propos</a></li>
           <li class="li_header_hideen"><a href="galerie.php">Galerie</a></li>
           <li class="li_header_hideen"><a href="contact.php">Contact</a></li>
           <li class="li_header_hideen"><a href="actualite.php">Actualites</a></li>
       </ul>
       </div>
   <script src="header.js"></script>
</header>
