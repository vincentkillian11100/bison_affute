
<?php
// On démarre la session (ceci est indispensable dans toutes les pages de notre section membre)
session_start();

// On récupère nos variables de session

?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="admin.css" />
    <link rel="apple-touch-icon" sizes="180x180" href="../dossier/favicon_io/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../dossier/favicon_io//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../dossier/favicon_io//favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <title>Espace administrateur</title>
  </head>
  <body>
      <!------------------- [ DEBUT page a propos ] ------------------->
      <?php
          include "../controleur/fonctions.php";
          $h1 = changeh1Propos()[0];
          $para1 = changePara1Propos()[0]; 
          $img1 = img1Propos()[0]; 
          $para2 = changePara2Propos()[0]; 
          $img2 = img2Propos()[0]; 
          $para3 = changePara3Propos()[0]; 
      ?>
<div id="montitre">
      <h1 id="titreadmin">ESPACE ADMINISTRATEUR</h1>
    </div>
<ul id="menuadmin">
<li><a href="#formulairePageAPropos"><p>A PROPOS</p></a></li>
      <div id="formulairePageAPropos">
          <p><b>Page a propos</b></p>
          <p><b>Modifier les informations</b></p>
    <form action="../controleur/propos.php" method="post" id="container">
      <label for="h1Propos">Modifier le titre</label>
      <input name="h1Propos" type="text" id="h1Propos" value="<?php echo $h1 ; ?>" />
      <label for="story1">Modifier le premier paragraphe :</label>
      <textarea autocomplete="off" placeholder="Écrivez le premier paragraphe ici..." id="story1" name="story1" rows="9"><?php echo $para1 ; ?></textarea
      >
      <label for="imgfile1">Choisir la premiere image</label>
      <p>( l'image doit se trouver dans le dossier bison_affute/images/ )</p>
      <input name="imgfile1" id="imgfile1" type="file" />
      <label for="story2">Modifier le second paragraphe:</label>
      <textarea placeholder="Modifier le second paragraphe ici..." id="story2" name="story2" rows="9"><?php echo $para2 ; ?></textarea
      >
      <label for="imgfile2">Choisir la seconde image</label>
      <p>( l'image doit se trouver dans le dossier bison_affute/images/ )</p>
      <input name="imgfile2" id="imgfile2" type="file" />
      <label for="story3">Modifier le troisième paragraphe:</label>
      <textarea id="story3" name="story3" rows="9" cols="100" placeholder="Modifier le troisième paragraphe ici..."><?php echo $para3 ; ?></textarea
      >
      <input type="submit" value="modifier" >
    </form>
</div>
<!------------------- [ FIN page a propos ] ------------------->


<!------------------- [ DEBUT formulaire pour ajouter une image ] ------------------->
<li><a href="#ajoutimage"><p>AJOUTER UNE IMAGE</p></a></li>
<div id="ajoutimage">
<form  class="column" action="../controleur/ajoutimage.php" method="post">
<h1>Ajouter une image</h1> 
<label for="nom">Nom de l'image</label>
<input id="nom" type="text" name="nom" required>

<label for="chemin">chemin</label>
<input id="chemin"type="file" name="chemin" required>

<input type="submit" value="ajouter">
</form>
</div>
<!------------------- [ FIN formulaire pour ajouter une image ] ------------------->

<!------------------- [ DEBUT formulaire pour modifier visibilité image ] ------------------->
<li><a href="#imgoff"><p>RENDRE IMAGE INVISIBLE</p></a></li>
<div id="imgoff">

<?php
$resultat = PrendreImagesVisibles();

?> 
<form class="column"action="../controleur/imageinvisible.php" method="post">
  <h1>Ne plus afficher l'image</h1>
<select name="" id="">
  <?php
foreach($resultat as $img){
?>
<option value="<?php $img["id"];?>">
<?php echo($img["chemin"]);?>
</option>
<?php } ?>
</select>  
<input class="btncommande" type="submit" value="Modifier">
</form>
</div>
<!------------------- [ FIN formulaire pour modifier visibilité image ] ------------------->
<!------------------- [ DEBUT formulaire pour ajouter une video ] ------------------->
<li><a href="#ajoutvideo"><p>AJOUTER UNE VIDEO</p></a></li>
<div id="ajoutvideo">
<form  class="column" action="../controleur/ajoutvideo.php" method="post">
<h1>Ajouter une video</h1> 

<label for="nom">Nom de la video</label>
<input id="nom" type="text" name="nom" required>

<label for="lien">chemin</label>
<input id="lien"type="text" name="lien" required>

<input type="submit" value="ajouter">
</form>
</div>
<!------------------- [ FIN formulaire pour ajouter une video] ------------------->

<!------------------- [ DEBUT formulaire pour MODIF VISI VIDEO] ------------------->
<li><a href="#videooff"><p>RENDRE VIDEO INVISIBLE</p></a></li>
<div id="videooff">

<?php
$resultat = PrendreVideosVisibles();

?> <form class="column"action="../controleur/videoinvisible.php" method="post">

  <h1>Ne plus afficher la video</h1>
<select name="" id="">
  <?php
foreach($resultat as $video){
?>
<option value="<?php $video["id"];?>">
<?php echo($video["nom"]);?>
</option>
<?php } ?>
</select>  
<input class="btncommande" type="submit" value="Modifier">
       
</form>
</div>
<!------------------- [ FIN formulaire pour MODIF VISI VIDEO] ------------------->

<!------------------- [ DEBUT formulaire pour modifier COORDONNEES ] ------------------->

<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
?>


<?php 



$resultat = getInfos();

?>
<li><a href="#modifcoord"><p>MODIFIER COORDONNEES</p></a></li>
    <div id="modifcoord">
       <form class="column"action="../controleur/modifcoord.php" method="post">
        <h1>Modifier coordonnées</h1>
    
        <label for="adresse">Adresse:</label>  
        <input type="text" name="adresse" value="<?php echo($resultat["adresse"]);?>">
          <label for="telephone">telephone:</label>
        <input type="text" name="telephone" value="<?php echo($resultat["telephone"]);?>">       
          <label for="mail">telephone:</label>
        <input type="text" name="mail" value="<?php echo($resultat["mail"]);?>">       
          <label for="horaires">horaire:</label>
          <input type="text" name="horaires" value="<?php echo($resultat["horaires"]);?>">
     <input class="btncommande" type="submit" value="Modifier">
       
</form>
</div>

        
<!------------------- [ FIN formulaire pour modifier COORDONNEES ] ------------------->


<!------------------- [DEBUT FORMULAIRE PRIX] ------------------->
<li><a href="#ajoutmodifprestation"><p>AJOUT PRESTATION</p></a></li>
<div id="ajoutmodifprestation"class="container_prix">
  <div class="childContainer_prix">
    <p>Ajouter une prestation</p>
<form class="" action="../controleur/AjoutPrix.php" method="post">
<label for="ajoutrubrique">Sélectionner la rubrique:</label>
      <select name="rubrique" type="text" id="rubrique" >
      <option value="couteaux">couteaux</option>
  <option value="ciseaux">ciseaux</option>
  <option value="travaildubois">bois</option>
  <option value="jardin">jardin</option>
  </select>
      <label for="ajouPrestation">Ajouter une prestation:</label>
      <input name="prestations" type="text" id="prestations" />
      <label for="ajoutPrix"> prix :</label>
      <input name="prix" type="text" id="h1Propos"/>
       <input type="submit" value="Ajouter">   
</form>


<form class=""action="../controleur/modifPrix.php" method="post">
      <label for="ajoutPrix">Ne plus afficher un service:</label>
      <input name="prestations" type="text" id="prestations" />
       <input type="submit" value="suppr">   
</form>
</div>
</div>
<!------------------- [FIN FORMULAIRE PRIX] ------------------->

<!------------------- [ DEBUT du formulaire ajouter un article page actu ] ------------------->
<li><a href="#containerSuppArticleTitre"><p>AJOUTER UN ARTICLE</p></a></li>
<div id="containerSuppArticleTitre">
            <p ><b>Page actualite</b></p>
            <form
                action="../controleur/actualite.php"
                method="post"
                id="container">
                <p><b>Ajouter un article</b></p>
                <label for="titreArticle">Titre de l'article</label>
                <input name="titreArticle" type="text" id="titreArticle" value="">
                <label for="dateArticleActu">Date de l'article</label>
                <input
                    name="dateArticleActu"
                    type="date"
                    id="dateArticleActu"
                    value="01 / 01 / 1970"
                />

                <label for="imageArticleActu">Choisir une image</label>
                <p>( l'image doit se trouver dans le dossier bison_affute/images/ )</p>
                <input
                    name="imageArticleActu"
                    id="imageArticleActu"
                    type="file"
                />
                <label for="texteDescriptionActu">Ajouter un paragraphe:</label>
                <textarea
                    placeholder="Ajouter un paragraphe ici..."
                    id="texteDescriptionActu"
                    name="texteDescriptionActu"
                    rows="9"
                    cols="100"
                ></textarea>

                <input type="submit" value="Ajouter" />
            </form>
        </div>
      
<li><a href="#supprimerarticles"><p>SUPPRIMER ARTICLES</p></a></li>
<div id="supprimerarticles">
<div id="containerSuppArticleTitre">
  <p><b>Page actualite</b></p>
  <p><b>Supprimer un article</b></p>
</div>
<main>
<?php
            $getAllIdActu = getAllIdActu();
        ?>

            <?php
    foreach ($getAllIdActu as $i) {
        ?>
        <form class="containerSupArticle" method="post" action="../controleur/supprimerActualite.php">
            <p class="titreActu2"><?php echo  $i["titreArticle"]; ?></p>
            <p class="dateActu"><?php echo  $i["texteDateActu"]; ?></p>
            <img class="imgActu" src="../images/<?php  echo $i["imageActu"]; ?>" alt="Image actualite">
            <p class="textActu">
                <?php echo $i["texteDescriptionActu"]; ?>
            </p>
            <input checked name="btnSuppArticle" type="radio" id="btnSuppArticle" value="<?php echo  $i["id"]; ?>">
            <br />
            <input type="submit" value="Supprimer" >
        </form>

        
<?php
    }?>

</main>
</div>
</ul>
<!------------------- [ FIN page supprimer un article ] ------------------->

</body>
</html>
