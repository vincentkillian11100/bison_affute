<div id="divTopFooterKK"></div>
        <footer>
            <div class="bigDivFooter">
                <div class="listFooter" id="containerLeftFooter">
                    <ul class="classUlFooter">
                        <li class = "liFooter"><a href="index.php">Accueil</a></li>
                        <li class = "liFooter"><a href="services.php">Mes services</a></li>
                        <li class = "liFooter"><a href="propos.php">A propos</a></li>
                        <li class = "liFooter"><a href="galerie.php">Galerie</a></li>
                        <li class = "liFooter"><a href="contact.php">Contact</a></li>
                        <li class = "liFooter"><a href="actualite.php">Actualites</a></li>
                    </ul>
                </div>
            </div>
            <div class="bigDivFooter">
                <div id="containerMiddleFooter1">
                    <div class="divMiddleLink">
                        <a href="https://www.facebook.com/Bison.affute11"
                            ><img
                            class ="logoFooterLink"
                                width="50"
                                src="../images/logo_facebook.png"
                                alt="logo_facebook
                            "
                        /></a>
                    </div>
                    <div class="divMiddleLink">
                        <a href="https://www.youtube.com/channel/UCCskTYJncIHcUikjWXHhaGw"
                            ><img
                            class ="logoFooterLink"
                                width="50"
                                src="../images/logo_youtube.png"
                                alt="logo_youtube
                            "
                        /></a>
                    </div>
                    <div class="divMiddleLink">
                        <a href="https://www.instagram.com/?hl=en"
                            ><img
                            class ="logoFooterLink"
                                width="50"
                                src="../images/logo_instagram.png"
                                alt="logo_instagram
                            "
                        /></a>
                    </div>
                </div>
                <div id="containerMiddleFooter2">
                    <div class="divMiddleCopyRight">
                        <p>Copyright 2022 Simplon, Narbonne</p>
                    </div>
                </div>
            </div>
            <div class="bigDivFooter" id="divFooterRightContact">
                <div class="containerRightFooter">
                    <p id="numFooter"><a href="tel:06.09.57.58.48">06.09.57.58.48</a></p>
                </div>
                <div class="containerRightFooter">
                    <p id="adresseFooter"><a href="https://goo.gl/maps/ACmyF2HPXL3sBj7B9">SAINT-NAZAIRE
                    D’AUDE</a></p>
                   
                </div>
            </div>
        </footer>
    </body>
</html>
