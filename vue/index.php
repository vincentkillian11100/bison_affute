<title><?php echo "Accueil"; ?></title>
<?php include('header.php'); ?>
<body class ="IndexBody">
    <div class= "head">
        <div class="container">
            <div class="intro-text">
                <div class="info">
                    <p class = "telTop"><a href="tel:06.09.57.58.48">06.09.57.58.48</a></p>
                </div>
                    <div class="intro-heading"></div>
                      <main class="index">
                        <h1 class="word1"><span class="blur1">Affutage toutes lames</span> </h1>
                         <h2 class="word2"><span class="blur1">et</span></h2>
                          <h3 class="word3"><span class="blur1">tout objet tranchant </span></h3>
                      </main>
                <i class='fas fa-angle-down arrow' style='font-size:36px; color:yellow'></i>
            </div>
        </div>
    </div>

    <div id="section1" class="container-fluid">
        <div class="mainMidle">
            <h3 class = "text1Index">Prenez rendez-vous et venez faire affûter vos outils dans la commune de SAINT-NAZAIRE, par un
                professionnel ! </h3>
                <hr>
            <h3 class = "text2Index">J’affute vos couteaux, ciseaux, ciseaux à bois, sécateurs, cisailles haies, taille-haies thermiques ou
                électriques,
                les lames de coupe jambon, les chaînes de tronçonneuse.</h3>
                <hr>

            <div class="container2">
                <div class="contact">
                    <button class="button"><a href="contact.php">contactez nous</a></button>           
                </div>

            </div>
            <img src="https://www.bisonaffute.fr/img/lames.png" id="lames" alt="">
        </div>
       
</div>

     <div id="section2" class="container-fluid">
        <h1 class="metier"><u>Où me trouver...</u></h1>
          <div class="flex">
             <div class="column">
                 <iframe title="Adresse de mon atelier" class = "iframeIndex"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1221.9867737380519!2d2.897210462092828!3d43.24323220259002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1b137e8485891%3A0x2d1d2d79ce3e5709!2zQmlzb24gQWZmw7t0w6ksIGFmZnV0ZXVyIHLDqW1vdWxldXI!5e0!3m2!1sfr!2sfr!4v1643364470812!5m2!1sfr!2sfr"
                     width="600" height="450"  allowfullscreen="" loading="lazy"></iframe>    
             </div>

                    <div class="top_index">
                      <div id="open"></div>
                        <div id="currently">
                          <p id="currently_open">Bison Affûté</p>
                        </div>

                        <div id="currently2">
                            <p class= "Semain">Lundi-Vendredi:</p>
                            <p class ="time1">09:00 - 12:00</p>
                            <p class = "time2">14:00 - 18:00</p>
                            <p class="red">Samedi: FERMÉ</p>
                            <p class="red">Dimanche:FERMÉ</p>
                                
                        </div>
                    </div>

                </div>
            
</div>

                <div id="section3" class="container-fluid">
                  <h1 class="metier"><u>Decouvrir mon métier</u></h1>
                   <div class="container3">
                            <?php 
                            include('../controleur/fonctions.php'); 

                            $resultat = afficherImgIndex();

                            ?>
                    
                            
                                <?php
                                foreach ($resultat as $i) {
                                ?>
                      <a href="galerie.php"><img class="imagesbdd" src="<?php echo"../images/". $i["chemin"]; ?>" alt="photofilm"></a>
                                
                                <?php } ?>
                                 
                               
                                </div>

  <?php

include('footer.php');

?>
    <script src="index.js"async></script>
    <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>   
</body>

</html>


