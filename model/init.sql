DROP USER IF EXISTS 'adminBison'@'localhost';
CREATE USER 'adminBison'@'localhost' IDENTIFIED BY 'Password123#@!';
GRANT ALL PRIVILEGES ON baseBison.* TO 'adminBison'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

DROP USER IF EXISTS 'adminBison2'@'localhost';
CREATE USER 'adminBison2'@'localhost' IDENTIFIED BY 'Password123#@!';
GRANT ALL PRIVILEGES ON baseBison.* TO 'adminBison2'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

DROP DATABASE IF EXISTS baseBison ;
CREATE DATABASE baseBison ;
USE baseBison ;

 CREATE TABLE Administrateur (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom varchar(255),
    mdp varchar(100)
);

CREATE TABLE images (
   	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   	nom VARCHAR(225),
	chemin VARCHAR(225),
   	visibilite BOOLEAN
);

CREATE TABLE videos (
   	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   	nom VARCHAR(225),
	lien VARCHAR(2083),
   	visibilite BOOLEAN
);


CREATE TABLE coordonnees (
   	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	adresse VARCHAR(225),
   	telephone VARCHAR(25),
	mail VARCHAR(225),
   	horaires VARCHAR(225)
	

);
CREATE TABLE services (
   	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   	texte TEXT(8000)
);

CREATE TABLE actualite (
   	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   	titreArticle TEXT(8000),
   	texteDateActu TEXT(8000),
   	imageActu VARCHAR(255),
   	texteDescriptionActu TEXT(8000),
	visibilite BOOLEAN
);
CREATE TABLE utilisateur (
   	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   	nom VARCHAR(50),
    mdp VARCHAR(50)
);
CREATE TABLE apropos (
   	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   	h1 VARCHAR(255),
   	para1 TEXT(6000),
    img1 VARCHAR(255),
   	para2 TEXT(6000),
    img2 VARCHAR(255),
   	para3 TEXT(6000)
);


CREATE TABLE Prix (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
rubrique VARCHAR (200),
prestations VARCHAR(2000),
prix VARCHAR(255),
visibilite BOOLEAN
);





INSERT INTO coordonnees (adresse,telephone,mail,horaires )

VALUES(

    "8 rue des oliviers 11120 St Nazaire d'Aude",
    "06 55 66 77 88",
	"franc@affute@gmail.com",
    "ouvert de 9h à 12h00 et de 14 à 18H"
);

INSERT INTO apropos (h1, para1, img1,para2,img2,para3) 
  
VALUES ("A propos de Bison Affûté","Métier d’antan, le rémoulage consiste à ré-affûter des lames afin qu'elles retrouvent leurs tranchant, couteaux,ciseaux, haches....
l’émouleur travaillait dans une coutellerie pour donner le premier aiguisage …","9B5AA4251FE4AE591EF8EA2BE7124.jpeg","J’entretiens, je répare vos lames abîmées et assure la longévité de votre matériel
Je donne une nouvelle vie à tous vos ciseaux, affûtage, rémoulage à la meule fine et à l’eau
Du sécateur à votre cisaille, sans oublier votre chaîne de tronçonneuse, je rénove vos vieux outils de jardin
Ce service est proposé sur la commune de SAINT-NAZAIRE D’AUDE, à mon atelier","91CCC4F2A17A9CBA89D73A7111829.jpg","Je peux aussi venir chez vous récupérer vos lames et les ramener."
);


INSERT INTO actualite (titreArticle,texteDateActu, imageActu,texteDescriptionActu,visibilite)
VALUES ("Création de ma chaine Youtube Bison Affûté !","01-02-2022", "abonnerYtb.png" , "Venez faire un tour 😉 https://www.youtube.com/channel/UCCskTYJncIHcUikjWXHhaGw", "1");
INSERT INTO actualite (titreArticle,texteDateActu, imageActu,texteDescriptionActu,visibilite)
VALUES ("Création de mon site web avec SIMPLON Occitanie !","01-02-2022", "simplon.png" , "👾👾👾 avec les apprenants de Simplon 👾👾👾.", "1");

INSERT INTO Administrateur (nom, mdp) VALUES ("nomadmin", "mdpadmin");
